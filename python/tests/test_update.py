from unittest import mock
import responses

import chrono

FAKED_API_KEY = "123"


@responses.activate
@mock.patch("socket.socket")
def test_update_nominal(socket_mock):
    chrono_title = "My Script"
    chrono_total = 100
    ts_id = '3rx6e6sJ5q4HCCG7'
    ts_json = {'id': ts_id, 'title': chrono_title, 'tags': [], 'description': None,
               'created': '2023-01-08T13:18:41.099015', 'processed': 0, 'done': None, 'total': chrono_total,
               'public': False, 'operation': 'processing'}
    timeseries_create_url = f"https://chrono.shodan.io/api/timeseries?key={FAKED_API_KEY}"
    responses.add(responses.POST, timeseries_create_url, json=ts_json, status=200)
    timeseries_done_url = f"https://chrono.shodan.io/api/timeseries/{ts_id}/done/{chrono_total}?key={FAKED_API_KEY}"
    responses.add(responses.PUT, timeseries_done_url)

    items = [i for i in range(chrono_total)]
    with chrono.progress(chrono_title, len(items), api_key=FAKED_API_KEY) as pb:
        assert pb.ts is not None
        for _ in items:
            pb.update()
        assert pb.processed == len(items)


@responses.activate
@mock.patch("socket.socket")
def test_update_invalid_ts_returned(socket_mock):
    chrono_title = "My Script"
    chrono_total = 100
    ts_invalid_json = {
        'error': 'Maximum number of progress bars reached. Delete a progress bar if you would like to create a new one.'
    }
    timeseries_create_url = f"https://chrono.shodan.io/api/timeseries?key={FAKED_API_KEY}"
    responses.add(responses.POST, timeseries_create_url, json=ts_invalid_json, status=200)

    items = [i for i in range(chrono_total)]
    with chrono.progress(chrono_title, len(items), api_key=FAKED_API_KEY) as pb:
        assert pb.ts is not None
        for _ in items:
            pb.update()
        assert pb.processed == len(items)
